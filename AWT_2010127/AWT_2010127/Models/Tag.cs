﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AWT_2010127.Models
{
    public class Tag
    {
        public int tagId { get; set; }
        public string tagName { get; set; }

        //Gets all the tags
        public List<string> getAllTags()
        { 
            var db = new QandASystemEntities();
            List<tag> tagEntities = db.tags.ToList();
            List<String> tagNames = new List<String>();
            foreach (tag t in tagEntities)
            {
                string name = t.tagName;
                tagNames.Add(name);
            }
            return tagNames;
        }
    }
}