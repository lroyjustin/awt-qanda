﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AWT_2010127.Models
{
    public class Question
    {
        public int questionId { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Title ")]
        public string title { get; set; }

        [Required]
        [Display(Name = "Description ")]
        public string description { get; set; }

        public DateTime created { get; set; }

        [Display(Name = "Category ")]
        public int categoryId { get; set; }

        public int userId { get; set; }

        [Display(Name = "Tags ")]
        public string tags { get; set; }

        public List<Answer> answers { get; set; }

        //Adds the question details to the db
        public bool AddQuestion(Question question)
        {
            using (var db = new QandASystemEntities())
            {
                var existingTitle = db.questions.FirstOrDefault(q => q.title == question.title);

                if (existingTitle != null)
                {
                    return false;
                }
                else
                {
                    var qn = db.questions.Create();

                    qn.title = question.title;
                    qn.description = question.description;
                    qn.created = question.created;
                    qn.categoryId = question.categoryId;
                    qn.userId = question.userId;
                    qn.tags = question.tags;

                    db.questions.Add(qn);
                    db.SaveChanges();

                    var pType = db.pointTypes.FirstOrDefault(pt => pt.pointId == 1);
                    int point = pType.pointValue;

                    var currentUser = db.users.FirstOrDefault(u => u.userId == question.userId);
                    currentUser.reputationPoint = currentUser.reputationPoint + point;
                    db.SaveChanges();
                    return true;
                }
            }
        }

        //Updates the question details in the db
        public bool EditQuestion(Question question)
        {
            using (var db = new QandASystemEntities())
            {
                var existingTitle = db.questions.FirstOrDefault(q => q.title == question.title && q.questionId != question.questionId);

                if (existingTitle != null)
                {
                    return false;
                }
                else
                {
                    var editedQn = db.questions.FirstOrDefault(q => q.questionId == question.questionId);

                    editedQn.title = question.title;
                    editedQn.description = question.description;
                    editedQn.created = question.created;
                    editedQn.categoryId = question.categoryId;
                    editedQn.tags = question.tags;

                    db.SaveChanges();
                    return true;
                }
            }
        }

        //Deletes the question details in the db
        public void DeleteQuestion(int qId)
        {
            using (var db = new QandASystemEntities())
            {
                var DeleteQn = db.questions.FirstOrDefault(q => q.questionId == qId);
                int uId = DeleteQn.userId;
                db.questions.Remove(DeleteQn);
                db.SaveChanges();

                var pType = db.pointTypes.FirstOrDefault(pt => pt.pointId == 1);
                int point = pType.pointValue;

                var currentUser = db.users.FirstOrDefault(u => u.userId == uId);
                currentUser.reputationPoint = currentUser.reputationPoint - point;
                if (currentUser.reputationPoint < 0)
                {
                    currentUser.reputationPoint = 0;
                }
                db.SaveChanges();
            }
        }

        //Get the question id by the question title
        public int getQuestionId(string title)
        {
            var db = new QandASystemEntities();
            int qId = db.questions.FirstOrDefault(q => q.title == title).questionId;
            return qId;
        }

        //Gets all the category
        public SelectList getCategories()
        {
            SelectList categories;
            var db = new QandASystemEntities();
            categories = new SelectList(db.categories, "categoryId", "categoryName", "1");

            return categories;
        }

        //Gets a list of categories
        public List<string> getAllCategories()
        {
            var db = new QandASystemEntities();
            var categoryEntites = db.categories;
            List<string> catNames = new List<string>();
            foreach (category c in categoryEntites)
            {
                catNames.Add(c.categoryName);
            }

            return catNames;
        }

        //Gets all the questions
        public List<Question> getAllQuestions()
        {
            var db = new QandASystemEntities();
            List<question> questionsEntities = db.questions.ToList();
            List<Question> questions = new List<Question>();
            foreach (question qe in questionsEntities)
            {
                Question q = new Question();
                q.questionId = qe.questionId;
                q.title = qe.title;
                q.description = qe.description;
                q.created = qe.created;
                q.categoryId = qe.categoryId;
                q.userId = qe.userId;
                q.tags = qe.tags;
                questions.Add(q);
            }
            return questions;
        }

        //Gets the questions by the keyword
        public List<Question> getByKeyword(string keyword)
        {
            var db = new QandASystemEntities();
            string[] keys = keyword.Split(new[] { '+' }, StringSplitOptions.RemoveEmptyEntries);
            List<question> questionsEntities = new List<question>();
            foreach(string key in keys)
            {
                questionsEntities.AddRange(db.questions.Where(q => q.title.Contains(key) || q.description.Contains(key)).ToList());
                questionsEntities = questionsEntities.Distinct().ToList();
            }

            List<Question> questions = new List<Question>(); 
            foreach (question qe in questionsEntities)
            {
                Question q = new Question();
                q.questionId = qe.questionId;
                q.title = qe.title;
                q.description = qe.description;
                q.created = qe.created;
                q.categoryId = qe.categoryId;
                q.userId = qe.userId;
                q.tags = qe.tags;
                questions.Add(q);
            }
            return questions;
        }

        //Gets the questions by category
        public List<Question> getByCategory(string cat)
        {
            var db = new QandASystemEntities();
            List<question> questionsEntities = db.questions.Where(q => q.category.categoryName == cat.Substring(1, cat.Length - 2)).ToList();

            List<Question> questions = new List<Question>();
            foreach (question qe in questionsEntities)
            {
                Question q = new Question();
                q.questionId = qe.questionId;
                q.title = qe.title;
                q.description = qe.description;
                q.created = qe.created;
                q.categoryId = qe.categoryId;
                q.userId = qe.userId;
                q.tags = qe.tags;
                questions.Add(q);
            }
            return questions;
        }

        //Gets the questions by tags
        public List<Question> getByTag(string tag)
        {
            var db = new QandASystemEntities();
            List<question> questionsEntities = db.questions.Where(q => q.tags.Contains(tag.Substring(1, tag.Length - 2))).ToList();

            List<Question> questions = new List<Question>();
            foreach (question qe in questionsEntities)
            {
                Question q = new Question();
                q.questionId = qe.questionId;
                q.title = qe.title;
                q.description = qe.description;
                q.created = qe.created;
                q.categoryId = qe.categoryId;
                q.userId = qe.userId;
                q.tags = qe.tags;
                questions.Add(q);
            }
            return questions;
        }

        //Gets the questions by user id
        public List<Question> getByUserId(string userId)
        {
            var db = new QandASystemEntities();
            int id = Convert.ToInt32(userId);
            List<question> questionsEntities = db.questions.Where(q => q.userId == id).ToList();

            List<Question> questions = new List<Question>();
            foreach (question qe in questionsEntities)
            {
                Question q = new Question();
                q.questionId = qe.questionId;
                q.title = qe.title;
                q.description = qe.description;
                q.created = qe.created;
                q.categoryId = qe.categoryId;
                q.userId = qe.userId;
                q.tags = qe.tags;
                questions.Add(q);
            }
            return questions;
        }

        //Gets all the question details
        public Question getQuestionDetails(int id)
        {
            var db = new QandASystemEntities();
            var q = db.questions.Find(id);
            Question question = new Question();
            question.questionId = q.questionId;
            question.title = q.title;
            question.description = q.description;
            question.created = q.created;
            question.tags = q.tags;
            question.categoryId = q.categoryId;
            question.userId = q.userId;
            List<Answer> qAnswers = new List<Answer>();
            foreach(var a in q.answers)
            {
                Answer ans = new Answer();
                ans.answerId = a.answerId;
                ans.description = a.description;
                ans.created = a.created;
                ans.userId = a.userId;
                ans.questionId = a.questionId;
                qAnswers.Add(ans);
            }
            question.answers = qAnswers;
            return question;
        }

        //Gets the author name by the user id
        public string getAuthorName(int userId) 
        {
            var db = new QandASystemEntities();
            var u = db.users.Find(userId);
            string authorName = u.firstname + " " + u.lastname;
            return authorName;
        }
        //Get the category name by the id

        public string getCategory(int id)
        {
            var db = new QandASystemEntities();
            var c = db.categories.Find(id);
            
            return c.categoryName;
        }

        //Gets the related questions
        public List<Question> getRelated(string id)
        {
            var db = new QandASystemEntities();
            int uId = Convert.ToInt32(id);
            string allSkiils = db.users.FirstOrDefault(u => u.userId == uId).skills;

            List<Question> questions = new List<Question>();
            if (allSkiils != null)
            {
                List<string> skills = allSkiils.Split(',').ToList();
                List<question> qEntities = db.questions.ToList();


                foreach (question q in qEntities)
                {
                    foreach (string skill in skills)
                    {
                        if (q.tags.Contains(skill))
                        {
                            Question question = new Question();
                            question.questionId = q.questionId;
                            question.title = q.title;
                            questions.Add(question);
                            break;
                        }
                    }
                }
            }
            return questions;
        }

        //Updates the like option
        public void updateLike(int qid, int uid)
        {
            var db = new QandASystemEntities();
            var like = db.likes.FirstOrDefault(l => l.userId == uid && l.questionId == qid);
            if (like == null)
            {
                var l = db.likes.Create();
                l.questionId = qid;
                l.userId = uid;
                db.likes.Add(l);
                db.SaveChanges();
            }
        }

        //Updates the unlike option
        public void updateUnLike(int qid, int uid)
        {
            var db = new QandASystemEntities();
            var like = db.likes.FirstOrDefault(l => l.userId == uid && l.questionId == qid);
            if (like != null)
            {
                db.likes.Remove(like);
                db.SaveChanges();
            }
        }

        //Checks the like option
        public bool checkLike(int qid, int uid)
        {
            var db = new QandASystemEntities();
            var like = db.likes.FirstOrDefault(l => l.userId == uid && l.questionId == qid);
            if (like != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}