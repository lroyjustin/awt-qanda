﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AWT_2010127.Models
{
    public class QuestionAnswer
    {
        public Question Question { get; set; }
        public Answer Answer { get; set; }
    }
}