﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AWT_2010127.Models
{
    public class UserLogin
    {
        [StringLength(45)]
        [Display(Name = "Username ")]
        public string username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 6)]
        [Display(Name = "Password ")]
        public string password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 6)]
        [Display(Name = "Password ")]
        public string newpassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("newpassword", ErrorMessage = "The password and confirmation password do not match")]
        [Display(Name = "Confirm Password ")]
        public string confirmPassword { get; set; }

        [Display(Name = "Remember Me ")]
        public bool rememberMe { get; set; }

        //Changes the password 
        public void changePass(UserLogin userlogin)
        {
            using (var db = new QandASystemEntities())
            {
                var user = db.users.FirstOrDefault(u => u.username == userlogin.username);
                System.Security.Cryptography.SHA1 sha = System.Security.Cryptography.SHA1.Create();
                string hashedPassword = System.Convert.ToBase64String(sha.ComputeHash(System.Text.UnicodeEncoding.Unicode.GetBytes(userlogin.newpassword)));

                user.password = hashedPassword;
                db.SaveChanges();
            }
        }

        //Checks wheather the login is valid
        public string IsValid(UserLogin userLogin)
        {
            string isValid = "false";

            using (var db = new QandASystemEntities())
            {
                var sysuser = db.users.FirstOrDefault(u => u.username == userLogin.username);
                if (sysuser != null)
                {
                    System.Security.Cryptography.SHA1 sha = System.Security.Cryptography.SHA1.Create();
                    string hashedPassword = System.Convert.ToBase64String(sha.ComputeHash(System.Text.UnicodeEncoding.Unicode.GetBytes(userLogin.password)));

                    if (sysuser.password == hashedPassword)
                    {
                        if (sysuser.status == 1)
                        {
                            isValid = "true";
                            HttpContext.Current.Session["USERID"] = sysuser.userId;
                            HttpContext.Current.Session["USERNAME"] = sysuser.username;
                            HttpContext.Current.Session["FIRSTNAME"] = sysuser.firstname;
                            HttpContext.Current.Session["LASTNAME"] = sysuser.lastname;
                            HttpContext.Current.Session["EMAIL"] = sysuser.email;
                            HttpContext.Current.Session["USERTYPE"] = sysuser.typeId;


                            if (userLogin.rememberMe)
                            {
                                HttpCookie cookie = new HttpCookie("SessionInfo");
                                cookie["sessionId"] = HttpContext.Current.Session.SessionID;
                                cookie.Expires = DateTime.Now.AddDays(7);
                                HttpContext.Current.Response.Cookies.Add(cookie);

                                var currentSession = db.sessions.Create();
                                currentSession.userId = sysuser.userId;
                                currentSession.firstname = sysuser.firstname;
                                currentSession.lastname = sysuser.lastname;
                                currentSession.email = sysuser.email;
                                currentSession.typeId = sysuser.typeId;
                                currentSession.username = sysuser.username;
                                currentSession.sessionId = HttpContext.Current.Session.SessionID;
                                db.sessions.Add(currentSession);
                                db.SaveChanges(); 
                            }
                        }
                        else
                        {
                            isValid = "locked";
                        }
                    }
                }
            }
            return isValid;
        }

        //Clears all the login data, Sessions, Cookies
        public void clearEntry(int id)
        {
            using (var db = new QandASystemEntities())
            {
                var currentSession = db.sessions.FirstOrDefault(s => s.userId == id);
                if (currentSession != null)
                {
                    db.sessions.Remove(currentSession);
                    db.SaveChanges(); 
                }
                HttpContext.Current.Session.Clear();

                HttpCookie cookie = HttpContext.Current.Request.Cookies["SessionInfo"];
                if (cookie != null)
                {
                    cookie.Expires = DateTime.Now.AddDays(-1);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
            }
        }

        //Gets the current session
        public void getSession()
        {
            using (var db = new QandASystemEntities())
            {
                HttpCookie cookie = HttpContext.Current.Request.Cookies["SessionInfo"];
                string sId = cookie["sessionId"].ToString();
                var existingSession = db.sessions.FirstOrDefault(s => s.sessionId == sId);

                if (existingSession != null)
                {
                    HttpContext.Current.Session["USERID"] = existingSession.userId;
                    HttpContext.Current.Session["USERNAME"] = existingSession.username;
                    HttpContext.Current.Session["FIRSTNAME"] = existingSession.firstname;
                    HttpContext.Current.Session["LASTNAME"] = existingSession.lastname;
                    HttpContext.Current.Session["EMAIL"] = existingSession.email;
                    HttpContext.Current.Session["USERTYPE"] = existingSession.typeId;

                    HttpContext.Current.Response.Cookies["SessionInfo"]["sessionId"] = HttpContext.Current.Session.SessionID;
                    HttpContext.Current.Response.Cookies["SessionInfo"].Expires = DateTime.Now.AddDays(7);

                    existingSession.sessionId = HttpContext.Current.Session.SessionID;
                    db.SaveChanges();
                }
            }
        }
    }
}