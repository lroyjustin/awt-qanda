﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AWT_2010127.Models
{
    public class UserEdit
    {
        public int userId { get; set; }

        [Required]
        [StringLength(45)]
        [Display(Name = "First Name ")]
        public string firstname { get; set; }

        [Required]
        [StringLength(45)]
        [Display(Name = "Last Name ")]
        public string lastname { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(45)]
        [Display(Name = "Email Address ")]
        public string email { get; set; }

        [Display(Name = "Skills ")]
        public string skills { get; set; }

        //Get the current user by user id
        public UserEdit getUser(int userid)
        {
            UserEdit currentUser = new UserEdit();
            using (var db = new QandASystemEntities())
            {
                var currentUserEntity = db.users.Find(userid);
                currentUser.userId = currentUserEntity.userId;
                currentUser.firstname = currentUserEntity.firstname;
                currentUser.lastname = currentUserEntity.lastname;
                currentUser.email = currentUserEntity.email;
                currentUser.skills = currentUserEntity.skills;
            }
            return currentUser;
        }

        //Updates the user information
        public bool Edit(UserEdit userEdit)
        {
            using (var db = new QandASystemEntities())
            {
                var existingEmail = db.users.FirstOrDefault(u => u.email == userEdit.email && u.userId != userEdit.userId);
                if (existingEmail != null)
                {
                    return false;
                }
                else
                {
                    var editedUser = db.users.FirstOrDefault(u => u.userId == userEdit.userId);
                    editedUser.firstname = userEdit.firstname;
                    editedUser.lastname = userEdit.lastname;
                    editedUser.email = userEdit.email;
                    editedUser.skills = userEdit.skills;

                    db.SaveChanges();
                    return true;
                }
            }
        }
    }
}