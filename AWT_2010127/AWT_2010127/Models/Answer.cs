﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AWT_2010127.Models
{
    public class Answer
    {
        public int answerId { get; set; }

        [Required]
        [Display(Name = "Description ")]
        public string description { get; set; }

        public DateTime created { get; set; }

        public int userId { get; set; }

        public int questionId { get; set; }

        //Adds the answer details to the db
        public bool AddAnswer(Answer answer)
        {
            using (var db = new QandASystemEntities())
            {
                var ans = db.answers.Create();

                ans.description = answer.description;
                ans.created = answer.created;
                ans.userId = answer.userId;
                ans.questionId = answer.questionId;

                db.answers.Add(ans);
                db.SaveChanges();

                var pType = db.pointTypes.FirstOrDefault(pt => pt.pointId == 2);
                int point = pType.pointValue;

                var currentUser = db.users.FirstOrDefault(u => u.userId == answer.userId);
                currentUser.reputationPoint = currentUser.reputationPoint + point;
                db.SaveChanges();

                return true;

            }
        }

        //Updates the answer details 
        public int EditAnswer(Answer answer)
        {
            using (var db = new QandASystemEntities())
            {
                var editedAn = db.answers.FirstOrDefault(a => a.answerId == answer.answerId);

                editedAn.description = answer.description;
                editedAn.created = answer.created;

                db.SaveChanges();
                return editedAn.questionId;
            }
        }

        //Deletes the answer details
        public void DeleteAnswer(int aId)
        {
            using (var db = new QandASystemEntities())
            {
                var DeleteAn = db.answers.FirstOrDefault(a => a.answerId == aId);
                int uId = DeleteAn.userId;
                db.answers.Remove(DeleteAn);
                db.SaveChanges();

                var pType = db.pointTypes.FirstOrDefault(pt => pt.pointId == 2);
                int point = pType.pointValue;

                var currentUser = db.users.FirstOrDefault(u => u.userId == uId);
                currentUser.reputationPoint = currentUser.reputationPoint - point;
                if (currentUser.reputationPoint < 0)
                {
                    currentUser.reputationPoint = 0;
                }
                db.SaveChanges();
            }
        }

        //Gets the full name from the user id
        public string getAuthorName(int userId)
        {
            var db = new QandASystemEntities();
            var u = db.users.Find(userId);
            string authorName = u.firstname + " " + u.lastname;
            return authorName;
        }

        //Gets the answer details
        public Answer getAnswerDetails(int id)
        {
            var db = new QandASystemEntities();
            var a = db.answers.Find(id);
            Answer answer = new Answer();
            answer.answerId = a.answerId;
            answer.description = a.description;
            answer.created = a.created;
            answer.userId = a.userId;
            answer.questionId = a.questionId;
            return answer;
        }

        //Gets a list of answers by the user id
        public List<Answer> getByUserId(string userId)
        {
            var db = new QandASystemEntities();
            int id = Convert.ToInt32(userId);
            List<answer> answersEntities = db.answers.Where(a => a.userId == id).ToList();

            List<Answer> answers = new List<Answer>();
            foreach (answer ans in answersEntities)
            {
                Answer a = new Answer();
                a.answerId = ans.answerId;
                a.description = ans.description;
                a.created = ans.created;
                a.userId = ans.userId;
                a.questionId = ans.questionId;
                answers.Add(a);
            }
            return answers;
        }
    }
}