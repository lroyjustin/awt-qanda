﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AWT_2010127.Models
{
    public class User
    {
        public int userId { get; set; }

        [Required]
        [StringLength(45)]
        [Display(Name = "First Name ")]
        public string firstname { get; set; }

        [Required]
        [StringLength(45)]
        [Display(Name = "Last Name ")]
        public string lastname { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(45)]
        [Display(Name = "Email Address ")]
        public string email { get; set; }

        [Required]
        [StringLength(45)]
        [Display(Name = "Username ")]
        public string username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 6)]
        [Display(Name = "Password ")]
        public string password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("password", ErrorMessage = "The password and confirmation password do not match")]
        [Display(Name = "Confirm Password ")]
        public string confirmPassword { get; set; }

        [Display(Name = "Points ")]
        public int reputationPoint { get; set; }

        [Display(Name = "Locked ")]
        public int status { get; set; }

        [Required]
        [Display(Name = "Type ")]
        public int typeId { get; set; }

        [Display(Name = "Skills ")]
        public string skills { get; set; }

        //Creates new user
        public string Register(User user)
        {
            string registerResult = "NULL";

            using (var db = new QandASystemEntities())
            {
                var existingUsername = db.users.FirstOrDefault(u => u.username == user.username);
                var existingEmail = db.users.FirstOrDefault(u => u.email == user.email);

                if (existingEmail != null)
                {
                    registerResult = "DuplicateEmail";
                }
                else if (existingUsername != null)
                {
                    registerResult = "DuplicateUsername";
                }
                else
                {
                    var sysUser = db.users.Create();

                    sysUser.username = user.username;
                    sysUser.firstname = user.firstname;
                    sysUser.lastname = user.lastname;
                    sysUser.email = user.email;
                    sysUser.skills = user.skills;
                    sysUser.status = 1;

                    System.Security.Cryptography.SHA1 sha = System.Security.Cryptography.SHA1.Create();
                    string hashedPassword = System.Convert.ToBase64String(sha.ComputeHash(System.Text.UnicodeEncoding.Unicode.GetBytes(user.password)));
                    sysUser.password = hashedPassword;

                    sysUser.reputationPoint = 0;
                    sysUser.typeId = user.typeId;

                    db.users.Add(sysUser);
                    db.SaveChanges();

                    registerResult = "Registered";
                }
            }
            return registerResult;
        }

        //Gets user by user id
        public User getUser(int userid)
        {
            User currentUser = new User();
            using (var db = new QandASystemEntities())
            {
                var currentUserEntity = db.users.Find(userid);
                currentUser.userId = currentUserEntity.userId;
                currentUser.firstname = currentUserEntity.firstname;
                currentUser.lastname = currentUserEntity.lastname;
                currentUser.email = currentUserEntity.email;
                currentUser.username = currentUserEntity.username;
                currentUser.password = currentUserEntity.password;
                currentUser.reputationPoint = currentUserEntity.reputationPoint;
                currentUser.skills = currentUserEntity.skills;
                currentUser.typeId = currentUserEntity.typeId;
                
            }
            return currentUser;
        }

        //Get the top user other than the admin
        public List<User> getTopUsers()
        {
            var db = new QandASystemEntities();
            List<user> usersEntities = db.users.OrderByDescending(u => u.reputationPoint).ToList();
            List<User> users = new List<User>();
            foreach (user ue in usersEntities)
            {
                if (ue.typeId != 3)
                {
                    User u = new User();
                    u.userId = ue.userId;
                    u.firstname = ue.firstname;
                    u.lastname = ue.lastname;
                    u.reputationPoint = ue.reputationPoint;
                    u.typeId = ue.typeId;
                    users.Add(u);
                }
            }
            return users;
        }

        //Gets all the users
        public List<User> getAllUsers()
        {
            var db = new QandASystemEntities();
            List<user> usersEntities = db.users.ToList();
            List<User> users = new List<User>();
            foreach (user ue in usersEntities)
            {
                if (ue.typeId != 3)
                {
                User u = new User();
                u.userId = ue.userId;
                u.firstname = ue.firstname;
                u.lastname = ue.lastname;
                u.email = ue.email;
                u.username = ue.username;
                u.status = ue.status;
                u.reputationPoint = ue.reputationPoint;
                u.typeId = ue.typeId;
                users.Add(u);
                }
            }
            return users;
        }

        //Locks the users by the user id list
        public void LockUsers(List<int> uIds)
        {
            var db = new QandASystemEntities();
            List<user> allUsers = db.users.ToList();
            foreach (user u in allUsers)
            {
                if (uIds != null)
                {
                    if (uIds.Contains(u.userId))
                    {
                        u.status = 0;
                    }
                    else
                    {
                        u.status = 1;
                    }
                }
                else
                {
                    u.status = 1;
                }
            }
            db.SaveChanges();
        }
    }
}