﻿using System.Web;
using System.Web.Mvc;

namespace AWT_2010127
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}