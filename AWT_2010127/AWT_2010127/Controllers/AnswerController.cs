﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AWT_2010127.Models;

namespace AWT_2010127.Controllers
{
    public class AnswerController : Controller
    {
        public string getAuthorName(Answer answer)
        {
            return answer.getAuthorName(answer.userId);
        }

        //
        // GET: /Answer/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Answer/Create

        [HttpPost]
        public ActionResult Create(Answer answer, int qId)
        {
            answer.userId = Convert.ToInt32(Session["USERID"]);
            answer.created = DateTime.Now;
            answer.questionId = qId;
            if (ModelState.IsValid)
            {
                answer.AddAnswer(answer);

            }
            return RedirectToAction("Details", "Question", new { id = answer.questionId });
        }

        //
        // GET: /Answer/Edit/5

        public ActionResult Edit(int id)
        {
            Answer answer = new Answer();
            answer = answer.getAnswerDetails(id);

            if (Convert.ToInt32(Session["USERID"]) == answer.userId)
            {
                return View(answer);
            }
            return RedirectToAction("Details", "Question", new { id = answer.questionId });
        }

        //
        // POST: /Answer/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, Answer answer)
        {
            if (ModelState.IsValid && Session["USERID"] != null)
            {
                answer.userId = Convert.ToInt32(Session["USERID"]);
                answer.created = DateTime.Now;
                answer.answerId = id;

                answer.questionId = answer.EditAnswer(answer);
                return RedirectToAction("Details", "Question", new { id = answer.questionId });
            }
            return View(answer);
        }

        //
        // GET: /Answer/Delete/5

        public ActionResult Delete(int id)
        {
            Answer answer = new Answer();
            answer = answer.getAnswerDetails(id);

            if (Convert.ToInt32(Session["USERID"]) == answer.userId)
            {
                answer.DeleteAnswer(id);
            }
            return RedirectToAction("Details", "Question", new { id = answer.questionId });
        }
    }
}
