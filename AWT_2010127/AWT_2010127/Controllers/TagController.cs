﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AWT_2010127.Models;

namespace AWT_2010127.Controllers
{
    public class TagController : Controller
    {
        //
        // GET: /Tag/

        public JsonResult Index()
        {
            Tag tag = new Tag();

            return Json(tag.getAllTags(), JsonRequestBehavior.AllowGet);
        }

    }
}
