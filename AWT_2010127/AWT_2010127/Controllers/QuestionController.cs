﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AWT_2010127.Models;

namespace AWT_2010127.Controllers
{
    public class QuestionController : Controller
    {
        //
        // GET: /Question/

        public ActionResult Index()
        {
            Question question = new Question();

            return View(question.getAllQuestions());
        }

        //Gets author name by question
        public string getAuthorName(Question question)
        {
            return question.getAuthorName(question.userId);
        }

        //Gets category name by question
        public string getCategory(Question question)
        {
            return question.getCategory(question.categoryId);
        }

        //
        // GET: /Question/Details/5

        public ActionResult Details(int id)
        {
            Question question = new Question();
            question = question.getQuestionDetails(id);
            Answer answer = new Answer();

            QuestionAnswer qAnda = new QuestionAnswer();
            qAnda.Question = question;
            qAnda.Answer = answer;
            ViewData["Author"] = question.getAuthorName(question.userId);
            ViewData["Category"] = question.getCategory(question.categoryId);
            return View(qAnda);
        }

        //
        // GET: /Question/Create

        [HttpGet]
        public ActionResult Create()
        {
            if (Session["USERID"] != null)
            {
                Question question = new Question();
                ViewBag.Categories = question.getCategories();
                return View();
            }
            return RedirectToAction("Login", "Auth");
        }

        //
        // POST: /Question/Create

        [HttpPost]
        public ActionResult Create(Question question)
        {
            if (ModelState.IsValid)
            {
                question.userId = Convert.ToInt32(Session["USERID"]);
                question.created = DateTime.Now;
                if (question.AddQuestion(question))
                {
                    int qId = question.getQuestionId(question.title);
                    return RedirectToAction("Details", "Question", new { id = qId });
                }
                else
                {
                    ModelState.AddModelError("", "Question title already exists.");
                }
            }
            ViewBag.Categories = question.getCategories();
            return View(question);
        }

        //
        // GET: /Question/Edit/5

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Question question = new Question();
            question = question.getQuestionDetails(id);

            if (Convert.ToInt32(Session["USERID"]) == question.userId)
            {
                ViewBag.Categories = question.getCategories();
                return View(question);
            }
            return RedirectToAction("Details", "Question", new { id = id });
        }

        //
        // POST: /Question/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, Question question)
        {
            if (ModelState.IsValid && Session["USERID"] != null)
            {
                question.userId = Convert.ToInt32(Session["USERID"]);
                question.created = DateTime.Now;
                question.questionId = id;
                if (question.EditQuestion(question))
                {
                    return RedirectToAction("Details", "Question", new { id = id });
                }
                else
                {
                    ModelState.AddModelError("", "Question title already exists.");
                }
            }
            ViewBag.Categories = question.getCategories();
            return View(question);
        }

        //
        // GET: /Question/Delete/5

        public ActionResult Delete(int id)
        {
            Question question = new Question();
            question = question.getQuestionDetails(id);

            if (Convert.ToInt32(Session["USERID"]) == question.userId)
            {
                foreach (Answer a in question.answers)
                {
                    a.DeleteAnswer(a.answerId);
                }
                question.DeleteQuestion(id);
                return RedirectToAction("Index", "Question");
            }
            return RedirectToAction("Details", "Question", new { id = id });
        }

        //Calls the view for the search fuctionality
        public ActionResult Search(string search)
        {
            return View();
        }

        //Updates the like
        public ActionResult UpdateLike(int id)
        {

            if (Session["USERID"] != null)
            {
                Question question = new Question();
                question.updateLike(id, Convert.ToInt32(Session["USERID"]));
                return Content("Liked");
            }
            return Content("Error");
        }

        //Updates the unlike
        public ActionResult UpdateUnLike(int id)
        {
            if (Session["USERID"] != null)
            {
                Question question = new Question();
                question.updateUnLike(id, Convert.ToInt32(Session["USERID"]));
                return Content("Unliked");
            }
            return Content("Error");
        }

        //Checks the like
        public ActionResult CheckLike(int id)
        {
            if (Session["USERID"] != null)
            {
                Question question = new Question();
                if (question.checkLike(id, Convert.ToInt32(Session["USERID"])))
                {
                    return Content("Liked");
                }
            }
            return Content("Error");
        }
    }
}
