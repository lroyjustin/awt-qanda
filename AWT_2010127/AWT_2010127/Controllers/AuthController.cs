﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AWT_2010127.Models;
using System.Web.Security;

namespace AWT_2010127.Controllers
{
    public class AuthController : Controller
    {
        //
        // GET: /Auth/

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult LogIn()
        {
            WriteCookie();
            if (Session["USERID"] == null)
            {
                return View();
            }
            return RedirectToAction("Index", "Question");
        }

        [HttpPost]
        public ActionResult LogIn(UserLogin userLogin)
        {
            string result = CheckCookiesEnabled();
            if (result == "true")
            {
                if (userLogin.IsValid(userLogin) == "true")
                {
                    return RedirectToAction("Index", "Question");
                }
                else if (userLogin.IsValid(userLogin) == "locked")
                {
                    ModelState.AddModelError("", "Sorry your account has been locked by the administrator!!");
                }
                else
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
            }
            else
            {
                ModelState.AddModelError("", result);
            }
            return View(userLogin);
        }

        [HttpGet]
        public ActionResult LogOut()
        {
            if (Session["USERID"] != null)
            {
                UserLogin userLogin = new UserLogin();
                userLogin.clearEntry(Convert.ToInt16(Session["USERID"]));
            }
            return RedirectToAction("Index", "Question");
        }

        public void checkSession()
        {
            UserLogin userLogin = new UserLogin();
            userLogin.getSession();
        }

        [HttpGet]
        public ActionResult changePassword()
        {
            if (Session["USERID"] != null)
            {
                return View();
            }
            return RedirectToAction("Index", "Question");
        }

        [HttpPost]
        public ActionResult changePassword(UserLogin userLogin)
        {
            userLogin.username = Session["USERNAME"].ToString();

            if (ModelState.IsValid)
            {
                if (userLogin.IsValid(userLogin) == "true")
                {
                    userLogin.changePass(userLogin);
                    return RedirectToAction("ProfilePage", "User");
                }
                else
                {
                    ModelState.AddModelError("", "Current password is not valid");
                }
            }
            return View(userLogin);
        }

        public string CheckCookiesEnabled()
        {
            string val = "";
            // Check if the browser supports cookies
            if (Request.Browser.Cookies)
            {
                //Check the existence of the test cookie
                HttpCookie cookie = Request.Cookies["TestCookie"];
                if (cookie == null)
                {
                    val = "Cookies are not enabled on your browser. Please enable cookies in your browser preferences to continue.";
                }
                else
                {
                    val = "true";
                }
            }
            else
            {
                val = "Browser doesn't support cookies. Please install one of the modern browser's that support cookies.";
            }
            return val;
        }

        public void WriteCookie()
        {
            // Create the test cookie object
            HttpCookie cookie = new HttpCookie("TestCookie", "1");
            Response.Cookies.Add(cookie);
        }
    }
}
