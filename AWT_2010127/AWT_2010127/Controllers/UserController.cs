﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AWT_2010127.Models;

namespace AWT_2010127.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Registration()
        {
            if (Session["USERID"] == null)
            {
                return View();
            }
            return RedirectToAction("Index", "Question");
        }

        [HttpPost]
        public ActionResult Registration(User user)
        {
            if (ModelState.IsValid)
            {
                string registrationResult = user.Register(user);
                if (registrationResult != "NULL")
                {
                    if (registrationResult == "DuplicateEmail")
                    {
                        ModelState.AddModelError("email", "Email address already exists. Please enter a different email address.");
                    }
                    if (registrationResult == "DuplicateUsername")
                    {
                        ModelState.AddModelError("userName", "User name already exists. Please enter a different user name.");
                    }
                    if (registrationResult == "Registered")
                    {
                        return RedirectToAction("Index", "Question");
                    }
                }
            }

            return View(user);
        }

        [HttpGet]
        public ActionResult ProfilePage(int id)
        {
            User user = new User();
            return View(user.getUser(id));
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            if (Convert.ToInt32(Session["USERID"]) == id)
            {
                UserEdit useredit = new UserEdit();
                useredit = useredit.getUser(id);
                return View(useredit);
            }
            return RedirectToAction("Index", "Question");
        }

        [HttpPost]
        public ActionResult Edit(int id, UserEdit userEdit)
        {
            if (ModelState.IsValid && Session["USERID"] != null)
            {
                userEdit.userId = id;
                if (userEdit.Edit(userEdit))
                {
                    return RedirectToAction("ProfilePage", "User", new { id = id });
                }
                else
                {
                    ModelState.AddModelError("email", "Email address already exists. Please enter a different email address.");
                }
            }
            return View(userEdit);
        }

        public ActionResult Manage()
        {
            if (Convert.ToInt32(Session["USERTYPE"]) == 3)
            {
                User user = new User();
                return View(user.getAllUsers());
            }
            else
            {
                return RedirectToAction("ProfilePage", "User", new { id = Convert.ToInt32(Session["USERID"]) });
            }
        }

        [HttpPost]
        public ActionResult LockUsers(List<int> userIdsToLock)
        {
            User user = new User();
            user.LockUsers(userIdsToLock);

            return RedirectToAction("Manage");
        }
    }
}
