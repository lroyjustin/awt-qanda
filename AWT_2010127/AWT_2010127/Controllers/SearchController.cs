﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AWT_2010127.Models;

namespace AWT_2010127.Controllers
{
    public class SearchController : ApiController
    {
        // GET api/search
        public List<Question> Get()
        {
            Question question = new Question();

            return (question.getAllQuestions());
        }

        // GET api/search/5
        public List<Question> GetQuestionByKey(string value)
        {
            Question question = new Question();

            return (question.getByKeyword(value));
        }

        public List<Question> GetQuestionByTag(string value)
        {
            Question question = new Question();

            return (question.getByTag(value));
        }

        public List<Question> GetQuestionByCategory(string value)
        {
            Question question = new Question();

            return (question.getByCategory(value));
        }

        public List<Question> GetQuestionByUserId(string value)
        {
            Question question = new Question();

            return (question.getByUserId(value));
        }

        public List<Question> GetRelatedQuestion(string value)
        {
            Question question = new Question();

            return (question.getRelated(value));
        }

        public List<Answer> GetAnswerByUserId(string value)
        {
            Answer answer = new Answer();

            return (answer.getByUserId(value));
        }

        public List<User> GetTopUsers()
        {
            User user= new User();

            return (user.getTopUsers());
        }

        public User GetUserById(string value)
        {
            User user = new User();

            return (user.getUser(Convert.ToInt32(value)));
        }

        public string GetCategory(string value)
        {
            Question question = new Question();

            return (question.getCategory(Convert.ToInt32(value)));
        }

        public List<string> GetAllCategories()
        {
            Question question = new Question();

            return (question.getAllCategories());
        }



        // POST api/search
        public void Post([FromBody]string value)
        {
        }

        // PUT api/search/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/search/5
        public void Delete(int id)
        {
        }
    }
}
